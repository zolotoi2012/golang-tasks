package main

import (
	"fmt"
	"time"
)

func producer(stream Stream, tweets chan *Tweet) {

	for {
		tweet, err := stream.Next()
		if err == ErrEOF {
			close(tweets)
			return
		}
		// TODO: use channel here

		tweets <- tweet
	}

}

func consumer(tweets chan *Tweet) {
	// TODO: use channel here
	for tweet := range tweets{
		go func(t *Tweet) {
			if t.IsTalkingAboutGo() {
				fmt.Println(t.Username, "\ttweets about golang")
			}

			fmt.Println(t.Username, "\tdoes not tweet about golang")
		}(tweet)
	}


	//for _, t := range tweets {
	//	if t.IsTalkingAboutGo() {
	//		fmt.Println(t.Username, "\ttweets about golang")
	//		continue
	//	}
	//
	//	fmt.Println(t.Username, "\tdoes not tweet about golang")
	//}
}

func main() {
	start := time.Now()
	stream := GetMockStream()

	// Modification starts from here
	// Hint: this can be resolved via channels
	// Producer
	tweets := make(chan *Tweet, 100)

	go producer(stream, tweets)
	// Consumer
	consumer(tweets)

	fmt.Printf("Process took %s\n", time.Since(start))
}