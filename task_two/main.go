// Hint 1: time.Ticker can be used to cancel function
// Hint 2: to calculate time-diff for Advanced lvl use:
//  start := time.Now()
//	// your work
//	t := time.Now()
//	elapsed := t.Sub(start) // 1s or whatever time has passed

package main

import "time"

// User defines the UserModel. Use this to check whether a User is a
// Premium user or not
type User struct {
	ID        int
	IsPremium bool    // can be used for 2nd level task. Premium users won't have 10 seconds limit.
	TimeUsed  float64 // in seconds
}

func (u *User) SetTimeUsed(time float64) {
	u.TimeUsed += time
}

// HandleRequest runs the processes requested by users. Returns false if process had to be killed
func HandleRequest(process func(), u *User) bool {
	if u.IsPremium {
		return true
	}

	done := make(chan bool)

	tick := time.NewTicker(time.Duration(10 - u.TimeUsed) * time.Second).C

	go func() {
		start := time.Now()
		process()
		done <- true
		t := time.Now()
		elapsed := t.Sub(start).Seconds()
		u.SetTimeUsed(elapsed)
	}()

	select {
	case <-tick:
		return false
	case <-done:
		return true
	}

	// TODO: you need to modify only this function and implement logic that will return false for 2 levels of tasks.
	//process() // put to goroutine, time.After, new channel for routine process + ticker, listen to routine + check time

	//ticker := time.NewTicker(1 * time.Second)
	//done := make(chan bool)
	//
	//if u.TimeUsed >= 10.0 {
	//	ticker.Stop()
	//	done <- true
	//	close(done)
	//}
	//
	//start := time.Now()
	//go func() {
	//	select {
	//	case <-done:
	//		return
	//	case t := <-ticker.C:
	//		elapsed := t.Sub(start)
	//		u.SetTimeUsed(elapsed.Seconds())
	//	}
	//}()

	//if u.TimeUsed >= 10.0 {
	//	ticker.Stop()
	//	done <- true
	//	close(done)
	//}

	//ticker := time.NewTicker(500 * time.Millisecond)
	//done := make(chan bool)
	//go func() {
	//	for {
	//		select {
	//		case <-done:
	//			return
	//		case t := <-ticker.C:
	//			fmt.Println("Tick at", t)
	//		}
	//	}
	//}()

	//time.Sleep(1600 * time.Millisecond)
	//ticker.Stop()
	//done <- true
	//fmt.Println("Ticker stopped")

	//go func() bool {
	//	start := time.Now()
	//	if u.IsPremium == false && u.TimeUsed == 10 {
	//		return false
	//	}
	//
	//	t := time.Now()
	//	elapsed := t.Sub(start)
	//	u.SetTimeUsed(elapsed.Milliseconds() * 1000)
	//	return true
	//}()
}

func main() {
	RunMockServer()
}
